use std::fs::File;

use image::io::Reader;

mod color;
mod ray;
mod vec3;

use std::fmt::Write;

use color::write_color;
use ray::Ray;
use vec3::{dot, Vec3};

fn hit_sphere(center: &Vec3, radius: f64, r: &Ray) -> bool {
    let oc = r.orig - center;
    let a = dot(&r.dir, &r.dir);
    let b = 2.0 * dot(&oc, &r.dir);
    let c = dot(&oc, &oc) - radius * radius;
    let discriminant = b * b - 4.0 * a * c;

    discriminant > 0.0
}

fn ray_color(r: Ray) -> Vec3 {
    if hit_sphere(&Vec3::new(0.0, 0.0, -1.0), 0.5, &r) {
        return Vec3::new(1.0, 0.0, 0.0);
    }

    let unit_direction: Vec3 = r.dir.unit_vector();
    let t = 0.5 * (unit_direction.y + 1.0);

    let white = Vec3::new(1.0, 1.0, 1.0);
    let blue = Vec3::new(0.5, 0.7, 1.0);

    (1.0 - t) * white + &(blue * t)
}

fn main() {
    //Image
    let aspect_ratio = 16.0 / 9.0;
    let image_width = 400;
    let image_height = (image_width as f64 / aspect_ratio) as u32;

    //Camera
    let viewport_height = 2.0;
    let viewport_width = aspect_ratio * viewport_height;
    let focal_lenght = 1.0;

    let origin = Vec3::new(0.0, 0.0, 0.0);
    let horizontal = Vec3::new(viewport_width, 0.0, 0.0);

    let vertical = Vec3::new(0.0, viewport_height, 0.0);
    let lower_left_corner: Vec3 =
        origin - &(horizontal / 2.0) - &(vertical / 2.0) - &Vec3::new(0.0, 0.0, focal_lenght);

    let mut output = String::new();

    write!(output, "P3\n{} {}\n255\n", image_width, image_height).unwrap();

    for j in (0..image_height).rev() {
        //eprintln!("Scnlines remaning: {}", j);
        //std::io::stderr().flush().unwrap();
        for i in 0..image_width {
            let u = (i as f64) / ((image_width - 1) as f64);
            let v = (j as f64) / ((image_height - 1) as f64);

            let r = Ray::new(
                origin,
                lower_left_corner + &(horizontal * u) + &(vertical * v) - &origin,
            );

            let pixel_color = ray_color(r);

            write_color(pixel_color, &mut output);
        }
    }

    let img = Reader::new(std::io::Cursor::new(output.as_bytes()))
        .with_guessed_format()
        .unwrap();

    img.decode().unwrap().save("test.jpg").unwrap();

    eprintln!("\nDone.");
}
