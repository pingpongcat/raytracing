use crate::vec3::Vec3;
use std::fmt::Write;

pub fn write_color(color: Vec3, output: &mut String) {
    let ir = (255.999 * color.x) as i32;
    let ig = (255.999 * color.y) as i32;
    let ib = (255.999 * color.z) as i32;

    write!(output, "{} {} {}\n", ir, ig, ib).unwrap();
}
