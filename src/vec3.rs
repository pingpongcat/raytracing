#[derive(Clone, Copy)]
pub struct Vec3 {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

impl Vec3 {
    pub fn new(x: f64, y: f64, z: f64) -> Self {
        Vec3 { x, y, z }
    }

    pub fn lenght_squared(&self) -> f64 {
        self.x * self.x + self.y * self.y + self.z * self.z
    }
    pub fn lenght(&self) -> f64 {
        self.lenght_squared().sqrt()
    }

    pub fn _make_uint_vector(&mut self) {
        let k = 1.0 / self.lenght_squared();

        self.x *= k;
        self.y *= k;
        self.z *= k;
    }

    pub fn unit_vector(&self) -> Self {
        let lenght = self.lenght();
        *self / lenght
    }
}

pub fn dot(u: &Vec3, v: &Vec3) -> f64 {
    u.x * v.x + u.y * v.y + u.z * v.z
}

impl std::fmt::Display for Vec3 {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "x: {}, y: {}, z: {}", self.x, self.y, self.z)
    }
}

impl std::ops::Neg for Vec3 {
    type Output = Self;

    fn neg(self) -> Self {
        Vec3 {
            x: -self.x,
            y: -self.y,
            z: -self.z,
        }
    }
}

impl std::ops::Add<&Self> for Vec3 {
    type Output = Self;

    fn add(self, other: &Self) -> Self {
        let x = self.x + other.x;
        let y = self.y + other.y;
        let z = self.z + other.z;

        Vec3 { x, y, z }
    }
}

impl std::ops::Sub<&Self> for Vec3 {
    type Output = Self;

    fn sub(self, other: &Self) -> Self {
        let x = self.x - other.x;
        let y = self.y - other.y;
        let z = self.z - other.z;

        Vec3 { x, y, z }
    }
}

impl std::ops::Mul<&Self> for Vec3 {
    type Output = Self;
    fn mul(self, t: &Self) -> Self {
        let x = self.x * t.x;
        let y = self.y * t.y;
        let z = self.z * t.z;

        Vec3 { x, y, z }
    }
}

impl std::ops::Mul<f64> for Vec3 {
    type Output = Self;

    fn mul(self, t: f64) -> Self {
        let x = self.x * t;
        let y = self.y * t;
        let z = self.z * t;

        Vec3 { x, y, z }
    }
}

impl std::ops::Mul<Vec3> for f64 {
    type Output = Vec3;

    fn mul(self, v: Vec3) -> Vec3 {
        let x = self * v.x;
        let y = self * v.y;
        let z = self * v.z;

        Vec3 { x, y, z }
    }
}

impl std::ops::Div<&Self> for Vec3 {
    type Output = Self;

    fn div(self, t: &Self) -> Self {
        let x = self.x / t.x;
        let y = self.y / t.y;
        let z = self.z / t.z;

        Vec3 { x, y, z }
    }
}

impl std::ops::Div<f64> for Vec3 {
    type Output = Self;

    fn div(self, t: f64) -> Self {
        self * (1.0 / t)
    }
}
